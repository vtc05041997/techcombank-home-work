package vn.techcombank;

import org.rapidoid.setup.On;
import vn.techcombank.api.HealthCheck;
import vn.techcombank.api.QueryPool;
import vn.techcombank.api.ReceivePool;
import vn.techcombank.util.Reports;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TechcombankService {
    public static void main(String[] args) {
        On.port(8101);
        On.post("/receivepool").plain(new ReceivePool());
        On.post("/querypool").plain(new QueryPool());
        On.post("/healthCheck").plain(new HealthCheck());

        ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(2);
        stpe.scheduleAtFixedRate(Reports::rpConsole, 0, 5, TimeUnit.SECONDS);
    }
}
