## TECHCOMBANK-HOME-WORK
## 1. FLOW
![image](images/Screenshot 2022-07-22 222225.png)
## 2. Project Structure
+ It contains 4 packages **Api**, **Handle**, **Model**, **Util**.
    + [api](src/main/java/vn/techcombank/api): This package handles data from client-site, and provides monitor.2 types of request **QUERYPOOL** and **RECEIVEPOOL** handle in this
    + [control](src/main/java/vn/techcombank/control): Create Map and handle the query from client requests.
    + [model](src/main/java/vn/techcombank/model): Response data to client
    + [util](src/main/java/vn/techcombank/util): It contains the reports console.
    
## 3. How To Run Project
+ Run in IDE: Go to the [entry point](src/main/java/vn/techcombank/TechcombankService.java) **src/main/java/vn/techcombank/TechcombankService.java** and RUN.
+ Build file jar and java -jar **pathToJarFile**
+ Port 8101
   + API1: http://localhost:8101/receivepool
        POST Request body example:
        
        {
             "poolId": 123546,
             "poolValues": [1,7,2,6]
         }
   + API2: http://localhost:8101/querypool
        POST Request body example:
        
        {
            "poolId": 123546,
            "percentile": 90.0
        }
        
## 4. Gitlab CICD AWS
![image](images/VNPT-CICD.png)