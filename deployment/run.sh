#!/bin/bash
echo 'Starting my app'

date_log=$(date '+%Y-%m-%d')'-tcb-home-work.log'
date_dir=$(date '+%Y-%m-%d')

mkdir -p /home/ec2-user/logs/$date_dir

nohup java -jar /home/ec2-user/run-app/techcombank-home-work-1.0-SNAPSHOT.jar > /home/ec2-user/logs/$date_dir/$date_log 2>&1 &