package vn.techcombank.control;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.techcombank.model.ResponseData;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertThat;

public class Handle {
    private static final Logger LOG = LoggerFactory.getLogger(Handle.class);
    public static ConcurrentHashMap<Integer, List<Integer>> myMap;
    static {
        try {
            myMap = new ConcurrentHashMap<>();
        }catch (Exception e){
            LOG.error(e.toString());
        }
    }
    public static String addToMap(Integer poolId, List<Integer> list){
        if(!myMap.containsKey(poolId)) {
            myMap.put(poolId,list);
            return "inserted";
        }
        else {
            List<Integer> old = myMap.get(poolId);
            old.addAll(list);
            myMap.put(poolId,old);
            return "appended";
        }
    }
    public static ResponseData percentileQuantiles(Double percentile, Integer poolId) {
        List<Integer> items = myMap.get(poolId);
        Collections.sort(items);
        Integer quantiles = items.get((int) Math.round(percentile / 100.0 * (items.size() - 1)));
        return new ResponseData(quantiles,items.size());
    }
}
