package vn.techcombank.api;

import com.google.gson.Gson;
import org.bson.Document;
import org.rapidoid.http.MediaType;
import org.rapidoid.http.Req;
import org.rapidoid.http.ReqHandler;
import org.rapidoid.http.Resp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.techcombank.control.Handle;
import vn.techcombank.model.ResponseData;

import java.util.List;

import static vn.techcombank.util.Reports.cs_req;

public class QueryPool implements ReqHandler {
    private static final Logger LOG = LoggerFactory.getLogger(QueryPool.class);

    @Override
    public Object execute(Req req) {
        Resp resp = req.response();
        resp.contentType(MediaType.TEXT_PLAIN);
        resp.header("Access-Control-Allow-Origin", "*");
        resp.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS");
        resp.code(200);
        Document body;
        Gson gson = new Gson();
        ResponseData responseData;
        cs_req.addAndGet(1);
        try {
            body = Document.parse(new String(req.body()));
            LOG.info(body.toString());
            Integer poolId = body.getInteger("poolId");
            Double percentile = body.getDouble("percentile");
            responseData = Handle.percentileQuantiles(percentile, poolId);
            resp.plain(gson.toJson(responseData));
            return resp;
        } catch (Exception e) {
            resp.code(400);
            responseData = new ResponseData("error: " + e.toString());
            resp.plain(gson.toJson(responseData));
            return resp;
        }
    }
}
