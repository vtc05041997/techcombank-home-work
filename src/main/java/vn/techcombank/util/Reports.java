package vn.techcombank.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.techcombank.api.ReceivePool;

import java.util.concurrent.atomic.AtomicLong;

public class Reports {

    private static final Logger LOG = LoggerFactory.getLogger(ReceivePool.class);
    public static AtomicLong cs_req = new AtomicLong(0);

    public static void rpConsole() {
        try {
            System.out.println("Request in 5s: " + cs_req.get());
            cs_req.set(0);
        } catch (Exception e) {
            LOG.error(e.toString());
        }
    }
}
