package vn.techcombank.api;

import com.google.gson.Gson;
import org.bson.Document;
import org.rapidoid.http.MediaType;
import org.rapidoid.http.Req;
import org.rapidoid.http.ReqHandler;
import org.rapidoid.http.Resp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static vn.techcombank.util.Reports.cs_req;

public class HealthCheck implements ReqHandler {
    private static final Logger LOG = LoggerFactory.getLogger(ReceivePool.class);
    @Override
    public Object execute(Req req) {
        Resp resp = req.response();
        cs_req.addAndGet(1);
        Gson gson = new Gson();
        resp.contentType(MediaType.TEXT_PLAIN);
        resp.header("Access-Control-Allow-Origin", "*");
        resp.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS");
        resp.code(200);
        resp.plain(gson.toJson("Status ok!"));
        return 200;
    }
}
