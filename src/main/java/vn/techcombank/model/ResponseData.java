package vn.techcombank.model;

public class ResponseData {
    private String message;
    private Integer quantile;
    private Integer count;

    public ResponseData(Integer quantile, Integer count) {
        this.quantile = quantile;
        this.count = count;
    }

    public ResponseData(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getQuantile() {
        return quantile;
    }

    public void setQuantile(Integer quantile) {
        this.quantile = quantile;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
